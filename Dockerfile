FROM gitlab-registry.cern.ch/akraszna/atlas-gpu-devel-env:centos7-1-4-0 

ADD afs/etc /usr/vice/etc

ADD cern.repo /etc/yum.repos.d

RUN sudo yum install -y krb5-workstation openafs-krb5
